package com.asics.b2b;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.swing.JOptionPane;

public class AsicsB2BNewSiteResourceFilesGenerater {

	private static final String LINE_SEPARATOR = System.getProperty("line.separator");
	private Properties parameterProperties;
	private String impexResourceTemplatePath;
	private String localPropertiesTemplatePath;
	private String ahqTemplatePath;
	private String hybrisImpexGeneratePath;
	private String hybrisLocalProtertiesPath;
	private String hybrisAHQCommonPath;
	
	private List<String> existCountries = Arrays.asList("ZAR", "AED", "USD", "EUR");
	
	public static void main(String[] args) {
		String title = "Generate New Site";
		int isGenerate = JOptionPane.showConfirmDialog(null, "Please make sure you are ready with parameter.properties file, and ready to start generating the new site.    ", title, JOptionPane.YES_NO_OPTION);
		if (isGenerate == JOptionPane.YES_OPTION) {
			AsicsB2BNewSiteResourceFilesGenerater siteGenerater = new AsicsB2BNewSiteResourceFilesGenerater();
			try {
				siteGenerater.generate();
				JOptionPane.showMessageDialog(null, "Generater Successful !", title, JOptionPane.INFORMATION_MESSAGE);
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null, String.format("Error: %s", e.getMessage()), title, JOptionPane.ERROR_MESSAGE);
			}
		}
	}
	
	public void generate() throws IOException {
		System.out.println("Copy start......");
		
		initVariable();
		appendAHQResource();
		generateHybrisImpexResource();
		appendHybrisLocalProperties();
		
		System.out.println("Copy end. ");
	}
	
	/**
	 * Copy all files under impexResource folder to specified path
	 * @throws IOException
	 */
	private void generateHybrisImpexResource() throws IOException {
		copyDir(this.impexResourceTemplatePath, this.hybrisImpexGeneratePath, false);
	}

	/**
	 * Copy specified file content under localProperties folder and append to specified path target file
	 * @throws IOException
	 */
	private void appendHybrisLocalProperties() throws IOException {
		appendHybrisLocalProperties("common", "common");
		appendHybrisLocalProperties("dev", "env/dev");
		appendHybrisLocalProperties("stage", "env/staging");
		appendHybrisLocalProperties("prod", "env/prod");
	}
	
	private void appendHybrisLocalProperties(String sourceName, String targetpath) throws IOException {
		String templatePropertiesPath = String.format("%s/%s-local.properties", this.localPropertiesTemplatePath, sourceName);
		String targetPropertiesPath = String.format("%s/%s/local.properties", this.hybrisLocalProtertiesPath, targetpath);
		copyFile(templatePropertiesPath, targetPropertiesPath, true);
	}
	
	private void appendAHQResource() throws IOException {
		if(!existCountries.contains(this.parameterProperties.get("defaultCurrency"))) {
			appendCurrencyData();
		}
		appendRegionsOfCountryData();
	}
	
	private void appendCurrencyData() throws IOException {
		String templatePath = String.format("%s/cmn-currency.impex", this.ahqTemplatePath);
		String targetPath = String.format("%s/cmn-currency.impex", this.hybrisAHQCommonPath);
		copyFile(templatePath, targetPath, true);
	}
	
	private void appendRegionsOfCountryData() throws IOException {
		String regionsOfCountry = (String) this.parameterProperties.get("regionsOfCountry");
		if (regionsOfCountry != null && !regionsOfCountry.isEmpty()) {
			String countryIso = (String) this.parameterProperties.get("countryIso");
			StringBuilder regionsImpex = new StringBuilder(LINE_SEPARATOR + "INSERT_UPDATE Region;country(isocode);isocode[unique=true];isocodeShort;name[lang=en];active[default=true]" + LINE_SEPARATOR);
			String[] regions = regionsOfCountry.split(";");
			for (int i = 0; i < regions.length; i++) {
				String[] strs = regions[i].split(":");
				if (strs.length == 2) {
					regionsImpex.append(String.format(";%s;%s-%s;%s;%s", countryIso, countryIso, strs[0], strs[0], strs[1]));
					regionsImpex.append(LINE_SEPARATOR);
				}
			}
			String targetPath = String.format("%s/cmn-country-region.impex", this.hybrisAHQCommonPath);
			try (FileWriter writer = new FileWriter(targetPath, true);) {
				writer.write(regionsImpex.toString().toCharArray());
				writer.flush();
			}
		}
	}

	private void copyDir(String sourcePath, String targetPath, boolean appendFile) throws IOException {
		File sourceDir = new File(sourcePath);
		if (!sourceDir.exists()) {
			throw new IOException("Not found directory: " + sourcePath);
		}
		String[] filePath = sourceDir.list();
		if (!(new File(targetPath)).exists()) {
			(new File(targetPath)).mkdir();
		}
		for (int i = 0; i < filePath.length; i++) {
			if ((new File(sourcePath + File.separator + filePath[i])).isDirectory()) {
				copyDir(sourcePath + File.separator + filePath[i], targetPath + File.separator + filePath[i], appendFile);
			}
			if (new File(sourcePath + File.separator + filePath[i]).isFile()) {
				copyFile(sourcePath + File.separator + filePath[i], targetPath + File.separator + filePath[i], appendFile);
			}
		}
	}

	/**
	 * @param sourcePath
	 * @param targetPath
	 * @param append true is append file content, false is replace the file.
	 * @throws IOException
	 */
	private void copyFile(String sourcePath, String targetPath, boolean append) throws IOException {
		try (BufferedReader bufReader = new BufferedReader(new InputStreamReader(new FileInputStream(new File(sourcePath))));
				FileWriter writer = new FileWriter(targetPath, append);) {
			StringBuilder strb = new StringBuilder();
			String line;
			while ((line = bufReader.readLine()) != null) {
				strb.append(line);
				strb.append(LINE_SEPARATOR);
			}
			writer.write(replacePlaceholders(strb.toString()).toCharArray());
			writer.flush();
		}
	}
	
	private String replacePlaceholders(String content) throws IOException {
		for (Map.Entry<Object, Object> entry : this.parameterProperties.entrySet()) {
			String vstr = String.format("#{%s}#", entry.getKey().toString());
			if (content.contains(vstr)) {
				content = content.replace(vstr, entry.getValue().toString());
			}
		}
		return content;
	}
	
	private Properties readParameterProperties(String parameterPropertiesPath) throws IOException {
		Properties properties = new Properties();
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader(parameterPropertiesPath))) {
			properties.load(bufferedReader);
			return properties;
		}
	}
	
	private void initVariable() throws IOException {
        String currentRootPath = System.getProperty("user.dir");
        this.parameterProperties = readParameterProperties(currentRootPath + "/parameter.properties");
        this.impexResourceTemplatePath = currentRootPath + "/resource/template/impexResource";
        this.localPropertiesTemplatePath = currentRootPath + "/resource/template/localProperties";
        this.ahqTemplatePath = currentRootPath + "/resource/template/AHQ";
        String hybrisRootPath = parameterProperties.getProperty("hybris.eCommerce.root.path");
        this.hybrisImpexGeneratePath =  String.format("%s/hybris/bin/custom/asics-b2b-ext/data/asicsb2bstoredata/resources/asicsb2bstoredata/import/%s", hybrisRootPath,  parameterProperties.getProperty("storeUid"));
        this.hybrisLocalProtertiesPath =  String.format("%s/config/hybris/", hybrisRootPath);
        this.hybrisAHQCommonPath = String.format("%s/hybris/bin/custom/asics-b2b-ext/data/asicsb2bstoredata/resources/asicsb2bstoredata/import/AHQ/common", hybrisRootPath);
	}

}
